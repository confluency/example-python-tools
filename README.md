# Example Python Repository for `git` training

This is an example repo that contains a small python module for common dataframe operations.

## Using these functions

There are two ways to use this library.
The first is that you can copy and paste into the `utils.py` file into your working directory as needed.
The second is to add this directory to your system path.

## Contributors

1. Paul Hobson (Confluency)
